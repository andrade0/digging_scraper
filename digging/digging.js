
module.exports = {
    define_search_type: function(query, get_search_type_callback){
        query = S(query).replaceAll('(', '').s;
        query = S(query).replaceAll(')', '').s;
        query = S(query).humanize().s;
        query = query.replace(/[^\w\s]/gi, '');
        query = query.toLowerCase();

        var discogs_results = [];

        async.waterfall([
            function(parallel_cb){
                var url = "https://www.discogs.com/search/?q="+encodeURI(query)+"&type=all";
                digging_curl(url, false, false, function(error, result){
                    if(result && result.html)
                    {
                        var $ = cheerio.load(result.html);
                        var count_artists = 0;
                        var count_label = 0;
                        var count_release = 0;
                        $('#search_results .card h4 a.search_result_title').each(function(k, v){

                            var elt = $(v).first();
                            var text = $(elt).text().trim().toLowerCase();
                            text.replace(new RegExp('\(.*\)','i'), '');
                            var href = $(elt).attr('href');
                            var img = $(elt).parent().prev().find('img').attr('data-src');

                            if(href.match('/artist/') && count_artists < 3 )
                            {
                                discogs_results.push({title: text, href: 'https://www.discogs.com'+href, img: img, type: 'artist'});
                                count_artists = count_artists + 1;
                            }
                            else if(href.match('/label/') && count_label < 3 )
                            {
                                discogs_results.push({title: text, href: 'https://www.discogs.com'+href, img: img, type: 'label'});
                                count_label = count_label + 1;
                            }
                            else if(href.match('/release/') && count_release < 3 )
                            {
                                discogs_results.push({title: text, href: 'https://www.discogs.com'+href, img: img, type: 'release'});
                                count_release = count_release + 1;
                            }

                        });
                        parallel_cb(null);
                    }
                });
            },
            function(parallel_cb){
                var url = "https://www.residentadvisor.net/search.aspx?searchstr="+encodeURI(query)+"&section=events&titles=1";
                digging_curl(url, false, false, function(error, result) {
                    if (result && result.html) {
                        var $ = cheerio.load(result.html);
                        var count_events = 0;
                        $('main ul.content-list.search li section.content div.generic.events div ul.list li a').each(function(k, v){

                            var elt = $(v).first();
                            var text = $(elt).text().trim().toLowerCase();
                            var href = $(elt).attr('href');
                            var img = null;

                            if(count_events < 3 )
                            {
                                discogs_results.push({title: text, href: 'https://www.discogs.com'+href, img: img, type: 'event'});
                                count_events = count_events + 1;
                            }
                        });
                    }
                    parallel_cb(null);
                });
            }
        ], function(result){
            get_search_type_callback(discogs_results);
        });


    },
    get_search_type: function(query, get_search_type_callback){
        query = S(query).replaceAll('(', '').s;
        query = S(query).replaceAll(')', '').s;
        query = S(query).humanize().s;
        query = query.replace(/[^\w\s]/gi, '');
        query = query.toLowerCase();

        async.waterfall([
            function(callback){
                var url = "https://www.discogs.com/search/?q="+query+"&type=all";
                digging_curl(url, false, false, callback);
            },
            function(result, callback)
            {
                if(result && result.html)
                {
                    var $ = cheerio.load(result.html);
                    var discogs_results = [];
                    $('#search_results .card h4 a.search_result_title').each(function(k, v){

                        var elt = $(v).first();
                        var text = $(elt).text().trim().toLowerCase();
                        var href = $(elt).attr('href');

                        if(href.match('/artist/') || href.match('/label/'))
                        {
                            discogs_results.push({title: text, href: href});
                        }
                    });

                    var options = {
                        keys: ['title'],
                        id: 'href'
                    };
                    var fuse = new Fuse(discogs_results, options);
                    var fuse_result = fuse.search(query);

                    if(fuse_result.length > 0)
                    {
                        var fuse_url_best_match = fuse_result[0];

                        if(fuse_url_best_match.match('/artist/'))
                        {
                            callback({search_type: 'artist'});
                        }
                        else if(fuse_url_best_match.match('/label/'))
                        {
                            callback({search_type: 'label'});
                        }
                        else
                        {
                            callback(null);
                        }
                    }
                    else
                    {
                        callback(null);
                    }
                }
                else
                {
                    callback(null);
                }
            }
        ], function(error, result) {
            var search_type = "all";
            if(error && error.search_type)
            {
                search_type = error.search_type;
            }
            else if(result && result.search_type)
            {
                search_type = result.search_type;
            }
            get_search_type_callback(search_type);
        });
    },
    prepare_release: function(source, doc){

        var $this = this;

        if(!doc.tms)
        {
            var tms = new Date().getTime();
            doc.tms = tms;
        }

        if(!doc.logo && source.logo )
        {
            doc.logo = source.logo;
        }

        if(!doc.item_type)
        {
            doc.item_type = 'release';
        }

        if(!doc.cover || (doc.cover && doc.cover.length <=0))
        {
            doc.cover = 'public/img/vinyl-digging-transparent.png';
        }

        if(doc.ep && !doc.title)
        {
            doc.title = doc.ep;
        }

        if(doc.artist)
        {
            doc.artist = doc.artist.trim().toLowerCase();
            doc.artist  = S(doc.artist).capitalize().s;
        }

        if(doc.played_by)
        {
            doc.played_by = doc.played_by.trim().toLowerCase();
            doc.played_by  = S(doc.played_by).capitalize().s;
        }

        if(doc.title)
        {
            doc.title = doc.title.trim().toLowerCase();
            doc.title  = S(doc.title).capitalize().s;
        }

        if(doc.genre)
        {
            doc.genre = doc.genre.trim().toLowerCase();
            doc.genre  = S(doc.genre).capitalize().s;
        }

        if(doc.label)
        {
            doc.label = doc.label.trim().toLowerCase();
            doc.label.replace('germany', '');
            doc.label.replace('france', '');
            doc.label.replace('japan', '');
            doc.label.replace('greece', '');
            doc.label.replace('spain', '');
            doc.label.replace('uk', '');
            doc.label.replace('italy', '');
            doc.label.replace('us', '');
            doc.label  = S(doc.label).capitalize().s;
        }

        doc.source = source.value.trim().toLowerCase();

        var keywords = [doc.title, doc.artist, doc.label, doc.genre, doc.played_by, doc.kind, doc.source];

        if(doc.url && doc.url.length && doc.url.length > 0 && doc.tracks && doc.tracks.length && doc.tracks.length > 0)
        {
            doc.tracks.forEach(function(v, k)
            {
                if(v.title && v.artist && v.artist.trim().length > 0 && v.title.trim().length > 0)
                {
                    v.artist = S(v.artist.trim()).capitalize().s;
                    v.title = S(v.title.trim()).capitalize().s;

                    if(!v.item_type)
                    {
                        v.item_type = 'track';
                    }

                    if(!v.logo && doc.logo )
                    {
                        v.logo = doc.logo;
                    }

                    v.release_title = doc.title;

                    v.tms = doc.tms;

                    if(doc.label && !v.label)
                    {
                        v.label = doc.label;
                    }
                    else if(v.label)
                    {
                        v.label = S(v.label.trim()).capitalize().s;
                    }

                    if(v.label)
                    {
                        v.label = v.label.trim().toLowerCase();
                        v.label.replace('germany', '');
                        v.label.replace('france', '');
                        v.label.replace('japan', '');
                        v.label.replace('greece', '');
                        v.label.replace('spain', '');
                        v.label.replace('uk', '');
                        v.label.replace('italy', '');
                        v.label.replace('us', '');
                        v.label  = S(v.label).capitalize().s;
                    }

                    if(!v.duration)
                    {
                        v.duration = null;
                    }

                    if(doc.release_date)
                    {
                        v.release_date = moment(doc.release_date).unix();
                    }
                    else
                    {
                        v.release_date = moment().unix();
                    }

                    if(doc.genre && !v.genre)
                    {
                        v.genre = doc.genre;
                    }
                    else if(v.genre)
                    {
                        v.genre = S(v.genre.trim()).capitalize().s;
                    }

                    if(doc.played_by && !v.played_by)
                    {
                        v.played_by = doc.played_by;
                    }
                    else if(v.played_by)
                    {
                        v.played_by = S(v.played_by.trim()).capitalize().s;
                    }

                    v.url = doc.url;

                    v.release_date = doc.release_date;
                    v.year = doc.year;
                    v.kind = doc.kind;
                    v.cover = doc.cover;
                    v.source = doc.source;
                    v.logo = source.logo;


                    if(!v.youtube)
                    {
                        v.youtube = '';
                    }

                    keywords.push(v.title);

                    if((doc.artist && v.artist) && doc.artist.toLowerCase().trim() != v.artist.toLowerCase().trim())
                    {
                        keywords.push(v.artist);
                    }

                    var keywords_track = [doc.title, doc.artist, doc.label, doc.genre, doc.played_by, doc.kind, doc.source, v.title, v.artist];

                    if(!v.query)
                    {
                        v.query = keywords_track.join(' ');
                    }

                    var alias = S(v.artist+'-'+doc.title+'-'+ v.title).latinise().s;
                    alias = S(alias).strip("'", '"', '!','`','*','£','ù','%','$','¨','°','(',')','§','[',']','{','}',',').s;
                    alias = S(alias).humanize().s;
                    alias = alias.toLowerCase();
                    alias = S(alias).replaceAll(' ', '-').s;
                    alias = S(alias).replaceAll('http://www.mixesdb.com/w/', '').s;

                    if(!v.alias)
                    {
                        v.alias = alias;
                    }

                    if(!v.id)
                    {
                        v.id = md5(v.alias+k);
                    }
                }
                else
                {
                    delete doc.tracks[k];
                }
            });

            if(!doc.query)
            {
                doc.query = keywords.join(' ');
            }

            if(doc.artist)
            {
                var alias = S(doc.artist+'-'+doc.title).latinise().s;
            }
            else
            {
                var alias = S(doc.title).latinise().s;
            }

            alias = S(alias).strip("'", '"', '!','`','*','£','ù','%','$','¨','°','(',')','§','[',']','{','}',',').s;
            alias = S(alias).humanize().s;
            alias = alias.toLowerCase();
            alias = S(alias).replaceAll(' ', '-').s;
            alias = S(alias).replaceAll('http://www.mixesdb.com/w/', '').s;

            if(!doc.alias)
            {
                doc.alias = alias;
            }

            doc.id = doc.alias;

            $this.insert_release_in_parse([doc]);

            return doc;
        }
        else
        {
            return null;
        }
    },
    insert_release_in_parse: function(releases){

      releases.forEach(function(release){
        releases.forEach(function(release){

          client.post('/insert', {release: release},function(){});

        });
      });

    },
    youtube: function(q, digging_youtube_search_callback){

        if(q && q.length && q.length > 0)
        {
            q = S(q).replaceAll('(', '').s;
            q = S(q).replaceAll(')', '').s;
            q = S(q).humanize('}', '').s;
            q = q.replace(/[^\w\s]/gi, '');

            var host = 'https://www.googleapis.com';
            var path = '/youtube/v3/search?part=id%2Csnippet&key=AIzaSyDZNXZkEYA_VzTcKDk52F7WgCOcqHT1pdc&q=';
            var url = host+path+q;
            digging_curl(url, false, false, function(err, res){
                if(res.html)
                {
                    var all_results = [];
                    var youtube_data = JSON.parse(res.html);

                    if(youtube_data && youtube_data.items && youtube_data.items.length > 0 && youtube_data.items[0])
                    {

                        youtube_data.items.forEach(function(item){

                            if(item.id.kind == 'youtube#video')
                            {
                                var the_title = item.snippet.title;

                                //if(digging_contains_all_words(q, the_title))
                                //{
                                    all_results.push({title: the_title, id: item.id.videoId});
                                //}
                            }
                        });

                        if(all_results.length > 0)
                        {
                            var options = {
                                /*threshold: 0.4,*/
                                keys: ['title','id']
                            };

                            var fuse = new Fuse(all_results, options);
                            var fuse_result = fuse.search(q);

                            if(fuse_result.length > 0)
                            {
                                var fuse_url_best_match = fuse_result[0];

                                console.log(fuse_url_best_match);
                                digging_youtube_search_callback(null, fuse_url_best_match);
                            }
                            else
                            {
                                digging_youtube_search_callback(null, null);
                            }
                        }
                        else
                        {
                            digging_youtube_search_callback(null, null);
                        }
                    }
                    else
                    {
                        digging_youtube_search_callback(null, null);
                    }
                }
                else
                {
                    digging_youtube_search_callback(null, null);
                }
            });
        }
        else
        {
            digging_youtube_search_callback(null, null);
        }
    }
};
