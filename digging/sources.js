var path = require('path');
var events = require('events');
var eventEmitter = new events.EventEmitter();
const _ = require('underscore');
const async = require( 'async' );
const S = require( 'string' );
const fs = require( 'fs' );
const cheerio = require( 'cheerio' );
const md5 = require( 'md5' );
const moment = require( 'moment' );
const Fuse = require( 'fuse.js' );
sources_path = path.join(__dirname.replace('modules/sources',''), 'sources');

module.exports = {
  sources: null,
  get: function(callback){
    var $this = this;
    if($this.sources === null)
    {
      $this.sources = [];
      async.each(fs.readdirSync(path.join(__dirname.replace('modules/sources',''), 'sources')), function(name, cb_each){
        var s = require(path.join(__dirname.replace('modules/sources',''), 'sources')+'/'+name);
        if(s.name)
        {
          $this.sources.push(s);
          cb_each(null);
        }
        else
        {
          cb_each(null);
        }
      }, function(error, result){
        callback($this.sources);
      });
    }
    else
    {
      callback($this.sources);
    }
  },
  getUserSources: function(callback){

    var $this = this;

    digging_storage.getUser(function(user){

      var user_sources = [];

      $this.get(function(sources){

        sources.forEach(function(source){

          if(user && user.sources && user.sources.length && user.sources.length > 0)
          {
            if(_.indexOf(user.sources, source.value) == -1)
            {

            }
            else
            {
              user_sources.push(source)
            }
          }
        });

        if(user_sources.length > 0)
        {
          callback(user_sources);
        }
        else
        {
          callback(sources);
        }
      });
    });
  }
};


