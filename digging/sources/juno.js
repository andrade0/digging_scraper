const cluster = require('../cluster.js');
const digging = require('../digging.js');
const _ = require('underscore');
const async = require( 'async' );
const S = require( 'string' );
const fs = require( 'fs' );
const cheerio = require( 'cheerio' );
const md5 = require( 'md5' );
const moment = require( 'moment' );
const Fuse = require( 'fuse.js' );

module.exports = {
    name: 'Juno',
    value: 'juno',
    //Return sting image uri
    logo: 'public/img/sources/juno.png',
    //query: string
    //search_artist_callback: function receiving array of release objects
    search_artist: function(query, search_artist_callback){
        var $this = this;
        var releases = [];
        async.waterfall([
            function(waterfall_callback)
            {
                var url = "http://www.juno.co.uk/search/?solrorder=relevancy&facet[stocked][0]=3&show_out_of_stock=1&q[artist][0]="+encodeURI(query);
                digging_curl(url, false, false, waterfall_callback);
            },
            function(result, waterfall_callback)
            {
                if(result.html)
                {
                    var releases_from_source = $this.parse_list(result.html);
                }
                else
                {
                    var releases_from_source = [];
                }

                if(releases_from_source.length > 0)
                {
                    async.each(releases_from_source, function(r, eachCallback){
                        var release = digging.prepare_release($this, r);

                        if(release)
                        {

                            releases.push(release);
                        }
                        eachCallback(null);
                    }, function(err, res){
                        waterfall_callback(null);
                    });
                }
                else
                {
                    waterfall_callback(null);
                }
            }], function(err){
            search_artist_callback(releases);
        });
    },
    //query: string
    //search_artist_callback: function receiving array of release objects
    search_label: function(query, search_artist_callback){
        var $this = this;
        var releases = [];
        async.waterfall([
            function(waterfall_callback)
            {
                var url = "http://www.juno.co.uk/search/?solrorder=relevancy&facet[stocked][0]=3&show_out_of_stock=1&q[label][0]="+encodeURI(query);
                digging_curl(url, false, false, waterfall_callback);
            },
            function(result, waterfall_callback)
            {
                if(result.html)
                {
                    var releases_from_source = $this.parse_list(result.html);
                }
                else
                {
                    var releases_from_source = [];
                }

                if(releases_from_source.length > 0)
                {
                    async.each(releases_from_source, function(r, eachCallback){
                        var release = digging.prepare_release($this, r);

                        if(release)
                        {

                            releases.push(release);
                        }
                        eachCallback(null);
                    }, function(err, res){
                        waterfall_callback(null);
                    });
                }
                else
                {
                    waterfall_callback(null);
                }
            }], function(err){
            search_artist_callback(releases);
        });
    },
    //query: string
    //search_artist_callback: function receiving array of release objects
    search_all: function(query, search_artist_callback){
        var $this = this;
        var releases = [];
        async.waterfall([
            function(waterfall_callback)
            {
                var url = "http://www.juno.co.uk/search/?solrorder=relevancy&facet[stocked][0]=3&show_out_of_stock=1&q[all][0]="+encodeURI(query);
                digging_curl(url, false, false, waterfall_callback);
            },
            function(result, waterfall_callback)
            {
                if(result.html)
                {
                    var releases_from_source = $this.parse_list(result.html);
                }
                else
                {
                    var releases_from_source = [];
                }

                if(releases_from_source.length > 0)
                {
                    async.each(releases_from_source, function(r, eachCallback){
                        var release = digging.prepare_release($this, r);

                        if(release)
                        {

                            releases.push(release);
                        }
                        eachCallback(null);
                    }, function(err, res){
                        waterfall_callback(null);
                    });
                }
                else
                {
                    waterfall_callback(null);
                }
            }], function(err){
            search_artist_callback(releases);
        });
    },
    //html : html source code
    parse_list: function(html){
        var $ = cheerio.load(html);

        var releases = [];

        if($('.dv-item').length > 0)
        {

            $('.dv-item').each(function(){
                var release_title = null;
                var release_year = null;
                var release_date = null;
                var release_cover = null;
                var release_label = null;
                var release_url = null;
                var release_genre = null;
                var release_artist = null;

                var src = $(this).find('div.pl-img').find('img').attr('src');
                if(src)
                {
                    if(src.match('data:image'))
                    {
                        var rel = $(this).find('div.pl-img').find('img').attr('rel');
                        if(rel)
                        {
                            release_cover = rel;
                        }
                    }
                    else
                    {
                        release_cover = src;
                    }
                }

                var artist_list = [];

                $(this).find('.pl-right').find('.pl-info').find('.vi-text').find('a').each(function(k, v){

                    if($(v).attr('href').match('/artists/'))
                    {
                        artist_list.push(S($(v).text()).capitalize().s.trim());
                    }

                    if($(v).attr('href').match('/labels/'))
                    {
                        release_label = S($(v).text()).capitalize().s.trim();
                    }

                    if($(v).attr('href').match('/products/'))
                    {
                        release_title = S($(v).text()).capitalize().s.trim();
                        release_url = 'http://www.juno.co.uk'+$(v).attr('href');
                    }
                });


                $(this).find('.pl-right').find('.pl-info').find('.vi-text').each(function(k, v){
                    if(k == 4)
                    {
                        release_genre = $(v).text().trim().replace('/', ', ');
                    }

                    if(k == 3)
                    {
                        var str = $(v).text().trim();
                        if(str.match(/Cat:/,'i') && str.match(/Rel:/,'i'))
                        {
                            var ex = str.split("Rel:");
                            if(ex.length > 0)
                            {
                                var date = ex[1].trim();
                                var ex1 = date.split(' ');
                                if(ex1.length > 2)
                                {
                                    if(parseInt(ex1[2])>60)
                                    {
                                        release_year = '19'+ex1[2];
                                    }
                                    else
                                    {
                                        release_year = '20'+ex1[2];
                                    }

                                    var month = ex1[1];
                                    var day = ex1[0];

                                    var months = [];
                                    months.push('Jan');
                                    months.push('Feb');
                                    months.push('Mar');
                                    months.push('Apr');
                                    months.push('May');
                                    months.push('Jun');
                                    months.push('Jul');
                                    months.push('Aug');
                                    months.push('Sep');
                                    months.push('Oct');
                                    months.push('Nov');
                                    months.push('Dec');

                                    var m = _.indexOf(months, month);

                                    if(m>0)
                                    {
                                        if(m < 10)
                                        {
                                            m = '0'+m;
                                        }
                                        release_date = release_year+'-'+m+'-'+day;
                                    }
                                }
                            }
                        }
                    }
                });

                if(artist_list.length < 1)
                {
                    release_artist = null;
                }
                else if(artist_list.length < 2)
                {
                    release_artist = artist_list[0];
                }
                else
                {
                    release_artist = artist_list.join(', ');
                }

                var tracks = [];

                $(this).find('.vi-tracklist').find('li').each(function(){
                    var t = {};
                    $(this).find('.vi-text').each(function(k, v){
                        var str = $(this).text().trim();
                        if(str)
                        {



                            var reg = new RegExp(/[(][0-9][:][0-9]{2}[)]/i);
                            var matches = str.match(reg);
                            if(matches && matches[0])
                            {
                                t.duration = matches[0];
                                t.duration = S(t.duration).replaceAll('(', '').s;
                                t.duration = S(t.duration).replaceAll(')', '').s;
                                var expl = str.split(' ');
                                var str2 = [];
                                expl.forEach(function(s)
                                {
                                    if(!s.trim().match(reg))
                                    {
                                        str2.push(s);
                                    }
                                });
                                str = str2.join(' ');
                            }

                            if(str.match(reg))
                            {

                            }

                            t.title = str;

                            var explode = t.title.split(' - "');

                            var matches = t.title.match(new RegExp(/[ ]["].+["]/));


                            if(explode.length > 1)
                            {
                                t.artist = explode[0];
                                t.title = explode[1];
                                t.title = S(t.title).replaceAll('"', '').s;
                            }
                            else if(matches)
                            {

                                t.title = S(matches[0]).replaceAll('"', '').s;
                                t.title = S(t.title).replaceAll(" ", "").s;
                                t.artist = matches.input.substr(0,matches.index);

                            }
                            else
                            {
                                t.artist = release_artist;
                            }

                        }
                    });
                    $(this).find('.vi-icon a.jrplayer').each(function(k, v){
                        t.sample = $(v).attr('href');
                    });
                    tracks.push(t);
                });

                if(tracks.length > 0)
                {
                    var tms = new Date().getTime();
                    releases.push({kind: 'release', artist: release_artist, tracks: tracks, label: release_label, cover: release_cover, source: 'juno', url: release_url, title: release_title, played_by: release_artist, year: release_year, release_date: release_date, genre: release_genre});
                }

            });
        }

        return releases;
    },
    get_news: function(query, get_news_callback){
        var $this = this;

        /*var feeds = [
            "http://www.juno.co.uk/all/this-week/?items_per_page=500&show_out_of_stock=1&media_type=vinyl",
            "http://www.juno.co.uk/deep-house/this-week/?items_per_page=500&show_out_of_stock=1&media_type=vinyl",
            "http://www.juno.co.uk/deep-house/this-week/?items_per_page=500show_out_of_stock=1&media_type=vinyl"
        ];*/
        var d = new Date().toJSON().slice(0,10);
        var feeds = [
            "http://www.juno.co.uk/all/this-week/?items_per_page=100&show_out_of_stock=1&media_type=vinyl&digging_date="+d,
            "http://www.juno.co.uk/deep-house/this-week/?items_per_page=100&show_out_of_stock=1&media_type=vinyl&digging_date="+d,
            "http://www.juno.co.uk/deep-house/this-week/?items_per_page=100show_out_of_stock=1&media_type=vinyl&digging_date="+d,
        ];

        all_juno = [];
        async.eachSeries(feeds, function(url, cb){
            digging_curl(url, false, false, function(error, result){
                if(result.html)
                {
                    var releases = $this.parse_list(result.html);

                    if(releases && releases.length && releases.length > 0)
                    {
                        releases.forEach(function(r){
                            var release = digging.prepare_release($this, r);
                            if(release)
                            {
                                all_juno.push(release);
                            }
                        });
                    }
                    cb(null);
                }
                else
                {
                    cb(null)
                }
            });
        }, function(e, r){
            get_news_callback(all_juno);
        });
    }
};
