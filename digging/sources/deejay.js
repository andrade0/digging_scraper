const cluster = require('../cluster.js');
const digging = require('../digging.js');
const _ = require('underscore');
const async = require( 'async' );
const S = require( 'string' );
const fs = require( 'fs' );
const cheerio = require( 'cheerio' );
const md5 = require( 'md5' );
const moment = require( 'moment' );
const Fuse = require( 'fuse.js' );

module.exports = {
    name: 'Deejay.de',
    value: 'deejay',
    logo: 'public/img/sources/deejay.png',
    search_artist: function(query,  search_artist_callback){

        var $this = this;
        var releases = [];
        async.waterfall([
            function(waterfall_callback)
            {
                var url = "http://www.deejay.de/content.php?param=/"+encodeURI(query)+"/perpage_500";
                digging_curl(url, false, false, waterfall_callback);
            },
            function(result, waterfall_callback)
            {
                if(result.html)
                {
                    var releases_from_source = $this.parse_list(result.html);
                }
                else
                {
                    var releases_from_source = [];
                }

                if(releases_from_source.length > 0)
                {
                    async.each(releases_from_source, function(r, eachCallback){

                        var release = digging.prepare_release($this, r);

                        if(release)
                        {
                            releases.push(release);
                        }
                        eachCallback(null);
                    }, function(err, res){
                        waterfall_callback(null);
                    });
                }
                else
                {
                    waterfall_callback(null);
                }
            }], function(err){
            search_artist_callback(releases);
        });
    },
    search_label: function(query,  search_artist_callback){
        var $this = this;
        $this.search_artist(query, search_artist_callback);
    },
    search_all: function(query, search_artist_callback){
        var $this = this;
        $this.search_artist(query, search_artist_callback);
    },
    parse_list: function(html){

        var $ = cheerio.load(html);

        var str = $('div#wrapper div#wrapcontent.column div#content h1.inline').first().text().trim();

        if(str)
        {
            var split = str.split(' : ');
            if(split && split.length && split.length == 2)
            {
                var query = split[1];
            }
        }

        var releases = [];

        if($('div#theList article.product').length > 0)
        {
            $('div#theList article.product').each(function(){
                var release_title = null;
                var release_year = null;
                var release_date = null;
                var release_cover = null;
                var release_label = null;
                var release_url = null;
                var release_genre = null;
                var release_artist = null;
                var release_source_id = null;

                var src = $(this).find('div.main_container div.cover div.img.img1 a.zoom').attr('href');
                if(src)
                {
                    //release_cover = 'http://www.deejay.de'+src;
                }

                var artist_list = [];

                var a = $(this).find('div.main_container div.inner_container div.inner_a div.artikel h2.artist interprets a').text().trim();

                if(a && a != 'Various Artists')
                {
                    artist_list.push(a);
                }

                var l = $(this).find('div.main_container div.inner_container div.inner_b div.label b').text().trim();
                if(l)
                {
                    release_label = l;
                }

                var sid = $(this).find('div.main_container div.inner_container div.inner_b div.label strong').text().trim();
                if(sid)
                {
                    release_source_id = sid;
                }

                var t = $(this).find('div.main_container div.inner_container div.inner_a div.artikel h3.title a').first();
                if(t)
                {
                    release_title = S(t.text()).capitalize().s.trim();
                    release_url = 'http://www.deejay.de'+t.attr('href');
                }

                var gs = [];
                $(this).find('div.product_footer div.product_footer_a div.style a.main').each(function(k, v){
                    var a = $(v);
                    gs.push(a.text());
                });

                if(gs.length > 0)
                {
                    release_genre = gs.join(', ');
                }

                var rd = $(this).find('div.product_footer div.product_footer_a div.date').text().trim().split('.');

                if(rd && rd.length && rd.length == 3)
                {
                    release_year = rd[2];
                    var month = rd[1];
                    var day = rd[0];
                    release_date = release_year+'-'+month+'-'+day;
                }

                if(artist_list.length < 1)
                {
                    release_artist = null;
                }
                else if(artist_list.length < 2)
                {
                    release_artist = artist_list[0];
                }
                else
                {
                    release_artist = artist_list.join(', ');
                }

                var tracks = [];

                $(this).find('div.main_container div.inner_container div.inner_a div.tracks ul.playtrack li a.track').each(function(k, v){
                    var t = {};
                    var elt = $(v);
                    var href = elt.attr('href');
                    var id = elt.attr('id');
                    if(href && id)
                    {
                        var sp = id.split('_');

                        if(sp.length && sp.length == 3)
                        {
                            var domid = sp[1];

                            if('play/__'+domid+'_'+sp[2] == href)
                            {
                                t.sample = 'http://www.deejay.de/streamit/'+domid[domid.length-2]+'/'+domid[domid.length-1]+'/'+domid+sp[2]+'.mp3';
                            }
                        }
                    }

                    var str = $(this).find("em").first().text().trim();

                    if(str)
                    {
                        t.title = str;

                        var explode = t.title.split(' - ');
                        var matches = t.title.match(new RegExp(/[ ]['].+[']/));

                        if(matches)
                        {
                            t.title = S(matches[0]).replaceAll("'", "").s;
                            t.title = S(t.title).replaceAll(" ", "").s;
                            t.artist = matches.input.substr(0,matches.index);
                        }
                        else if(explode.length > 1)
                        {
                            t.artist = explode[0];
                            t.title = explode[1];
                        }
                        else
                        {
                            t.artist = release_artist;
                        }

                        tracks.push(t);
                    }
                });

                if(tracks.length > 0)
                {
                    releases.push({kind: 'release', artist: release_artist, tracks: tracks, label: release_label, cover: release_cover, source: 'deejay.de', url: release_url, title: release_title, played_by: release_artist, year: release_year, release_date: release_date, genre: release_genre});
                }
            });
        }
        return releases;
    },
    get_news: function(query, get_news_callback){

        var all = [];
        var $this = this;
        var d = new Date().toJSON().slice(0,10);
        var url = 'http://www.deejay.de/content.php?param=/m_All/sm_News?digging_date='+d;

        digging_curl(url, false, false, function(error, result)
        {
            if(result.html)
            {
                var releases = $this.parse_list(result.html);
                if(releases && releases.length && releases.length > 0)
                {
                    releases.forEach(function(r){
                        var release = digging.prepare_release($this, r);
                        if(release)
                        {
                            all.push(release);
                        }
                    });
                    get_news_callback(all);
                }
                else
                {
                    get_news_callback([]);
                }
            }
            else
            {
                get_news_callback([]);
            }
        });
    }
};
