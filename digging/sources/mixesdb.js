const cluster = require('../cluster.js');
const digging = require('../digging.js');
const _ = require('underscore');
const async = require( 'async' );
const S = require( 'string' );
const fs = require( 'fs' );
const cheerio = require( 'cheerio' );
const md5 = require( 'md5' );
const moment = require( 'moment' );
const Fuse = require( 'fuse.js' );

module.exports = {
    name: 'MixesDb',
    value: 'mixesdb',
    //Return sting image uri
    logo: 'public/img/sources/mixesdb.png',
    //query: string
    //search_artist_1_callback: function, receiving array of object, ex: [{url: 'http://xxxx', x: y, ...}, {url: 'http://xxxx', x: y, ...}, ....]
    search_artist: function(query, href, search_artist_callback){
        var $this = this;
        var pages = [];

        async.waterfall([
            function(waterfall_callback){
                var url = "http://www.mixesdb.com/db/index.php?title=Special:Search&limit=1000&profile=all&search="+encodeURI(query);
                digging_curl(url, false, false, waterfall_callback);
            },
            function(result, waterfall_callback) {

                if(result.html)
                {

                    var $ = cheerio.load(result.html);

                    $('.mw-search-results .mw-search-result-heading a').each(function(k, v){

                        var href = "http://www.mixesdb.com"+$(v).attr('href');
                        var title = "http://www.mixesdb.com"+$(v).attr('title');

                        if(href){
                            pages.push({  url: href, title: title});
                        }
                    });

                    $('ul#catMixesList li a').each(function(k, v){

                        var href = "http://www.mixesdb.com"+$(v).attr('href');
                        var title = "http://www.mixesdb.com"+$(v).attr('title');

                        if(href){
                            pages.push({  url: href, title: title});
                        }
                    });
                    waterfall_callback(null);
                }
            }
        ], function(error, result){

            search_artist_callback(pages);
        });
    },
    search_all: function(query, href, search_artist_callback){
        var $this = this;
        var pages = [];

        async.waterfall([
            function(waterfall_callback){
                var url = "http://www.mixesdb.com/db/index.php?title=Special:Search&limit=1000&profile=all&search="+encodeURI(query);
                digging_curl(url, false, false, waterfall_callback);
            },
            function(result, waterfall_callback) {
                if(result.html)
                {
                    var $ = cheerio.load(result.html);

                    $('.mw-search-results .mw-search-result-heading a').each(function(k, v){

                        var href = "http://www.mixesdb.com"+$(v).attr('href');
                        var title = "http://www.mixesdb.com"+$(v).attr('title');

                        if(href){
                            pages.push({  url: href, title: title});
                        }
                    });

                    $('ul#catMixesList li a').each(function(k, v){

                        var href = "http://www.mixesdb.com"+$(v).attr('href');
                        var title = "http://www.mixesdb.com"+$(v).attr('title');

                        if(href){
                            pages.push({  url: href, title: title});
                        }
                    });
                    waterfall_callback(null);
                }
            }
        ], function(error, result){
            search_artist_callback(pages);
        });
    },
    //query: string
    //page: Object (ex: {url: 'http://xxxx', x: y, ....}
    //search_artist_1_callback: function, receiving release object or null
    search_artist_1: function(query, page, search_artist_1_callback){
        var $this = this;
        async.waterfall([
            function(waterfall_callback)
            {
                digging_curl(page.url, false, true, waterfall_callback);
            },
            function(result, waterfall_callback)
            {
                if(result.release)
                {
                    var release = digging.prepare_release($this, result.release);
                    waterfall_callback(release);
                }
                else if(result.html)
                {
                    var r = $this.parse_detail(result.html, page.url);

                    if(r)
                    {
                        var release = digging.prepare_release($this, r);

                        if(release)
                        {
                            waterfall_callback(release)
                        }
                        else
                        {
                            waterfall_callback(null);
                        }
                    }
                    else
                    {
                        waterfall_callback(null);
                    }
                }
                else
                {
                    waterfall_callback(null);
                }
            }
        ], function(err){
            search_artist_1_callback(err);
        });
    },
    search_all_1: function(query, page, search_artist_1_callback){
        var $this = this;
        async.waterfall([
            function(waterfall_callback)
            {
                digging_curl(page.url, false, true, waterfall_callback);
            },
            function(result, waterfall_callback)
            {
                if(result.release)
                {
                    waterfall_callback(result.release)
                }
                else if(result.html)
                {
                    var r = $this.parse_detail(result.html, page.url);

                    if(r)
                    {
                        var release = digging.prepare_release($this, r);

                        if(release)
                        {
                            waterfall_callback(release)
                        }
                        else
                        {
                            waterfall_callback(null);
                        }
                    }
                    else
                    {
                        waterfall_callback(null);
                    }
                }
                else
                {
                    waterfall_callback(null);
                }
            }
        ], function(err){
            search_artist_1_callback(err);
        });
    },
    //html : html source code
    //page_url : String (url of scraped page)
    parse_detail: function(html, url){
        var $ = cheerio.load(html);
        var cover = null;
        var label = null;
        var played_by = null;
        var source = "mixesdb";
        var tracks = [];
        var year = null;
        var release_date = null;
        var played_by = null;

        var ep = $('head title').text();

        var matches = ep.match(new RegExp(/[0-9]{4}[-][0-9]{2}[-][0-9]{2}/i));
        if(matches)
        {
            ep = ep.replace(new RegExp(/[0-9]{4}[-][0-9]{2}[-][0-9]{2}/i), '');
            release_date = matches[0].trim();
        }

        var matches = ep.match(new RegExp(/[0-9]{2}[:][0-9]{2}/i));
        if(matches)
        {
            ep = ep.replace(new RegExp(/[0-9]{2}[:][0-9]{2}/i), '');
        }

        var matches = ep.match(new RegExp(/[\[][?]+[\]]/i));
        if(matches)
        {
            ep = ep.replace(new RegExp(/[\[][?]+[\]]/i), '');
        }

        ep = S(ep).replaceAll('-', ' ').s;
        ep = S(ep).replaceAll('−', ' ').s;
        ep = ep.replace('MixesDB', '');
        ep = S(ep).replaceAll('  ', ' ').s;

        $('#catlinks #mw-normal-catlinks ul li a').each(function(k, v){
            if(k == 0)
            {
                year = v.children[0].data;
            }

            if(k == 1)
            {
                played_by = v.children[0].data;
            }
        });


        $('ol li').each(function(k, v){

            var str = $(this).text().trim();
            str = str.replace(/(\[)\d+(:)\d+(\])/,'');
            str = str.replace(/(\[)\d+(\])/ ,'');
            str = str.replace(/\[[0-9]\:[[0-9]{2}\:[[0-9]{2}\]/ ,'');
            str = str.replace(/[0-9]{4}\-[[0-9]{2}\-[[0-9]{2}/ ,'');
            str = str.replace(/[0-9]{2}\:[[0-9]{2}\:[[0-9]{2}/ ,'');

            if(str == '?' || str.length < 1)
            {

            }
            else
            {
                var ex = str.split(' - ');
                if(ex.length == 2)
                {
                    artist = ex[0];
                    title = ex[1];
                    if(_.where(tracks, {artist: artist, title: title }) <= 0)
                    tracks.push({  artist: artist, title: title });
                }
            }
        });

        if(tracks.length < 1)
        {
            $('#Tracklist + div.list .list-track').each(function(k,v){

                var str = $(v).text().trim();

                str = str.replace(/(\[)\d+(:)\d+(\])/,'');
                str = str.replace(/(\[)\d+(\])/ ,'');
                str = str.replace(/\[[0-9]\:[[0-9]{2}\:[[0-9]{2}\]/ ,'');
                str = str.replace(/[0-9]{4}\-[[0-9]{2}\-[[0-9]{2}/ ,'');
                str = str.replace(/[0-9]{2}\:[[0-9]{2}\:[[0-9]{2}/ ,'');
                str = str.trim();

                if(str == '?' || str.length < 1)
                {

                }
                else
                {
                    var ex = str.split(' - ');
                    if(ex.length == 2)
                    {
                        artist = ex[0];
                        title = ex[1];
                        if(_.where(tracks, {artist: artist, title: title }) <= 0)
                        tracks.push({  artist: artist, title: title });
                    }
                }
            });
        }


        if(tracks.length < 1)
        {
            $('div.list .list-track').each(function(k,v){

                var str = $(v).text().trim();
                str = str.replace(/(\[)\d+(:)\d+(\])/,'');
                str = str.replace(/(\[)\d+(\])/ ,'');
                str = str.replace(/\[[0-9]\:[[0-9]{2}\:[[0-9]{2}\]/ ,'');
                str = str.replace(/[0-9]{4}\-[[0-9]{2}\-[[0-9]{2}/ ,'');
                str = str.replace(/[0-9]{2}\:[[0-9]{2}\:[[0-9]{2}/ ,'');
                str = str.trim();

                if(str == '?' || str.length < 1)
                {

                }
                else
                {
                    var ex = str.split(' - ');
                    if(ex.length == 2)
                    {
                        artist = ex[0];
                        title = ex[1];
                        if(_.where(tracks, {artist: artist, title: title }) <= 0)
                        tracks.push({  artist: artist, title: title });
                    }
                }
            });
        }

        if(tracks.length < 1)
        {
            $('div#bodyContent.mw-body-content div#mw-content-text.linkPreviewWrapperList ol li').each(function(k,v){

                var str = $(v).text().trim();

                str = str.replace(/(\[)\d+(:)\d+(\])/,'');
                str = str.replace(/(\[)\d+(\])/ ,'');
                str = str.replace(/\[[0-9]\:[[0-9]{2}\:[[0-9]{2}\]/ ,'');
                str = str.replace(/[0-9]{4}\-[[0-9]{2}\-[[0-9]{2}/ ,'');
                str = str.replace(/[0-9]{2}\:[[0-9]{2}\:[[0-9]{2}/ ,'');
                str = str.trim();

                if(str == '?' || str.length < 1)
                {

                }
                else
                {
                    var ex = str.split(' - ');
                    if(ex.length == 2)
                    {
                        artist = ex[0];
                        title = ex[1];

                        if(_.where(tracks, {artist: artist, title: title }) <= 0)
                        tracks.push({artist: artist, title: title });
                    }
                }
            });
        }

        if(tracks.length>0)
        {
            return {kind: 'mix', release_date: release_date, artist: null, tracks: tracks, label: label, cover: cover, source: source, url: url, title: ep, played_by: played_by, year: year, genre: 'Electronic music'};

        }
        else
        {
            return null;
        }
    },
    parse_list: function(html){
        var $ = cheerio.load(html);
        var data = [];


        $('ul.linkPreviewWrapperList a.cat-tlC').each(function(k, v){
            if(v.attribs.href.length > 0)
            {
                var link =  "http://www.mixesdb.com"+v.attribs.href;
                if(v.attribs.title.length > 0)
                {
                    var ep =  v.attribs.title;
                }
                else
                {
                    var ep =  null;
                }
                data.push({  url: link, title: ep});
            }
        });

        return data;
    },
    _get_news: function(get_news_callback){
        var $this = this;
        var all = [];

        digging_curl('http://www.mixesdb.com/db/index.php?title=Main_Page&show=mixesfresh', false, false, function(error, result){
            if(result.html)
            {
                var pages = $this.parse_list(result.html);

                async.eachSeries(pages, function(page, eachSeriesCb){
                    digging_curl(page.url, true, true, function(error, result){
                        if(result.release)
                        {
                            all.push(result.release);
                        }
                        else if(result.html)
                        {
                            var r = $this.parse_detail(result.html, page.url);
                            if(r)
                            {
                                var release = digging.prepare_release($this, r);
                                if(release)
                                {
                                    all.push(release);
                                }
                            }
                        }
                        eachSeriesCb(null);
                    });
                },function(err, res){
                    get_news_callback(all);
                });
            }
            else
            {
                get_news_callback([]);
            }
        });
    }
};
