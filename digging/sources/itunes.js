const cluster = require('../cluster.js');
const digging = require('../digging.js');
const _ = require('underscore');
const async = require( 'async' );
const S = require( 'string' );
const fs = require( 'fs' );
const cheerio = require( 'cheerio' );
const md5 = require( 'md5' );
const moment = require( 'moment' );
const Fuse = require( 'fuse.js' );

module.exports = {
    name: 'Itunes',
    //Return sting image uri
    value: 'itunes',
    logo: 'public/img/sources/itunes.png',
    //query: string
    //search_artist_callback: function receiving array of release objects
    search_artist: function(query, search_artist_callback){
        var $this = this;
        var releases = [];
        async.waterfall([
            function(waterfall_callback)
            {
                var url = "https://itunes.apple.com/search?media=music&entity=musicTrack&limit=500&term="+encodeURI(query);
                digging_curl(url, false, false, waterfall_callback);
            },
            function(result, waterfall_callback)
            {
                if(result.html)
                {
                    var releases_from_source = $this.parse_list(result.html);
                }
                else
                {
                    var releases_from_source = [];
                }

                if(releases_from_source.length > 0)
                {
                    async.each(releases_from_source, function(r, eachCallback){

                        var release = digging.prepare_release($this, r);

                        if(release)
                        {
                            releases.push(release);
                        }
                        eachCallback(null);
                    }, function(err, res){
                        waterfall_callback(null);
                    });
                }
                else
                {
                    waterfall_callback(null);
                }
            }], function(err){
            search_artist_callback(releases);
        });
    },
    //query: string
    //search_artist_callback: function receiving array of release objects
    search_all: function(query, search_artist_callback){
        var $this = this;
        var releases = [];
        async.waterfall([
            function(waterfall_callback)
            {
                var url = "https://itunes.apple.com/search?media=music&entity=musicTrack&limit=500&term="+encodeURI(query);
                digging_curl(url, false, false, waterfall_callback);
            },
            function(result, waterfall_callback)
            {
                if(result.html)
                {
                    var releases_from_source = $this.parse_list(result.html);
                }
                else
                {
                    var releases_from_source = [];
                }

                if(releases_from_source.length > 0)
                {
                    async.each(releases_from_source, function(r, eachCallback){

                        var release = digging.prepare_release($this, r);

                        if(release)
                        {
                            releases.push(release);
                        }
                        eachCallback(null);
                    }, function(err, res){
                        waterfall_callback(null);
                    });
                }
                else
                {
                    waterfall_callback(null);
                }
            }], function(err){
            search_artist_callback(releases);
        });
    },
    //html : html source code
    parse_list: function(html){
        var data = JSON.parse(html);
        var releases = [];
        var releases_index = {};
        var all_tracks = [];
        if(data.results && data.results.length > 0)
        {
            data.results.forEach(function(d) {

                var release_source_id = d.collectionId;
                var trackId = d.trackId;
                var release_date = d.releaseDate;
                var exp = release_date.split('-');
                var exp1 = release_date.split('T');

                if(d.collectionViewUrl && (trackId != release_source_id))
                {
                    all_tracks.push({
                        release_title : d.collectionArtistName,
                        track_title : d.trackName,
                        release_year : exp[0],
                        release_date : exp1[0],
                        release_cover : d.artworkUrl30,
                        release_label : null,
                        release_url : d.collectionViewUrl,
                        release_genre : d.primaryGenreName,
                        track_artist : d.artistName,
                        release_source_id : release_source_id,
                        track_source_id : d.trackId
                    });
                }
            });
        }

        all_tracks.forEach(function(t){
            var rid = t.release_source_id;

            if(!releases_index[rid])
            {
                releases_index[rid] = {
                    release_artist : t.track_artist,
                    release_title : t.release_title,
                    release_year : t.release_year,
                    release_date : t.release_date,
                    release_cover : t.release_cover,
                    release_label : t.release_label,
                    release_url : t.release_url,
                    release_genre : t.release_genre,
                    release_source_id : t.release_source_id
                };
                releases_index[rid].tracks = [];
            }
            releases_index[rid].tracks.push({
                artist: t.track_artist,
                title: t.track_title
            });
        });



        for(var rid in releases_index) {
            var r = releases_index[rid];
            releases.push({kind: 'release', artist: r.release_artist, tracks: r.tracks, label: r.release_label, cover: r.release_cover, source: 'itunes', url: r.release_url, title: r.release_title, played_by: null, year: r.release_year, release_date: r.release_date, genre: r.release_genre});
        }

        return releases;
    }
    /*get_news: function(){

    }*/
};
