const cluster = require('../cluster.js');
const digging = require('../digging.js');
const _ = require('underscore');
const async = require( 'async' );
const S = require( 'string' );
const fs = require( 'fs' );
const cheerio = require( 'cheerio' );
const md5 = require( 'md5' );
const moment = require( 'moment' );
const Fuse = require( 'fuse.js' );


module.exports = {
    name: 'Beatport',
    value: 'beatport',
    logo: 'public/img/sources/beatport.png',
    search_artist: function(query, search_artist_callback){
        var $this = this;
        var releases = [];
        async.waterfall([
            function(waterfall_callback)
            {
                var url = "https://www.beatport.com/search?q="+encodeURI(query);
                digging_curl(url, false, false, waterfall_callback);
            },
            function(result, waterfall_callback)
            {
                var url = null;
                if(result.html)
                {
                    var beatport_results = [];
                    var $ = cheerio.load(result.html);
                    $('main.all-search-results div.artists ul.bucket-items li.bucket-item > a').each(function(k, v){
                        var title = $(v).find('p').text();
                        var href = 'https://www.beatport.com'+$(v).attr('href')+'/tracks?per-page=150&sort=release-desc';
                        beatport_results.push({title: title.toLowerCase().trim(), href: href});
                    });
                    var options = {
                        keys: ['title'],
                        id: 'href'
                    };
                    var fuse = new Fuse(beatport_results, options);
                    var fuse_result = fuse.search(query);

                    if(fuse_result.length > 0 && fuse_result[0])
                    {
                        url = fuse_result[0];
                    }

                    waterfall_callback(null, url);
                }
                else
                {
                    waterfall_callback(true);
                }
            },
            function(url, waterfall_callback)
            {
                if(url)
                {
                    digging_curl(url, false, false, waterfall_callback);
                }
                else
                {
                    waterfall_callback(true);
                }
            },
            function(result, waterfall_callback)
            {
                if(result.html)
                {
                    var releases_from_source = $this.parse_list(result.html);
                }
                else
                {
                    var releases_from_source = [];
                }

                if(releases_from_source.length > 0)
                {
                    async.each(releases_from_source, function(r, eachCallback){

                        var release = digging.prepare_release($this, r);

                        if(release)
                        {
                            releases.push(release);
                        }
                        eachCallback(null);
                    }, function(err, res){
                        waterfall_callback(null);
                    });
                }
                else
                {
                    waterfall_callback(null);
                }
            }], function(err){
            search_artist_callback(releases);
        });
    },
    search_label: function(query, search_artist_callback){
        var $this = this;
        var releases = [];
        async.waterfall([
            function(waterfall_callback)
            {
                var url = "https://www.beatport.com/search?q="+encodeURI(query);
                digging_curl(url, false, false, waterfall_callback);
            },
            function(result, waterfall_callback)
            {
                var url = null;
                if(result.html)
                {
                    var beatport_results = [];
                    var $ = cheerio.load(result.html);
                    $('main.all-search-results div.labels ul.bucket-items li.bucket-item > a').each(function(k, v){
                        var title = $(v).find('p').text();
                        var href = 'https://www.beatport.com'+$(v).attr('href')+'/tracks?per-page=150&sort=release-desc';
                        beatport_results.push({title: title.toLowerCase().trim(), href: href});
                    });
                    var options = {
                        keys: ['title'],
                        id: 'href'
                    };

                    var fuse = new Fuse(beatport_results, options);
                    var fuse_result = fuse.search(query);

                    if(fuse_result.length > 0 && fuse_result[0])
                    {
                        url = fuse_result[0];
                    }

                    waterfall_callback(null, url);
                }
                else
                {
                    waterfall_callback(true);
                }
            },
            function(url, waterfall_callback)
            {
                digging_curl(url, false, false, waterfall_callback);
            },
            function(result, waterfall_callback)
            {
                if(result.html)
                {
                    var releases_from_source = $this.parse_list(result.html);
                }
                else
                {
                    var releases_from_source = [];
                }

                if(releases_from_source.length > 0)
                {
                    async.each(releases_from_source, function(r, eachCallback){

                        var release = digging.prepare_release($this, r);

                        if(release)
                        {
                            releases.push(release);
                        }
                        eachCallback(null);
                    }, function(err, res){
                        waterfall_callback(null);
                    });
                }
                else
                {
                    waterfall_callback(null);
                }
            }], function(err){
            search_artist_callback(releases);
        });
    },
    search_all: function(query, search_artist_callback){
        var $this = this;
        var releases = [];
        async.waterfall([
            function(waterfall_callback)
            {
                var url = 'https://www.beatport.com/search/tracks?q='+encodeURI(query)+'&per-page=150';
                digging_curl(url, false, false, waterfall_callback);
            },
            function(result, waterfall_callback)
            {
                if(result.html)
                {
                    var releases_from_source = $this.parse_list(result.html);
                }
                else
                {
                    var releases_from_source = [];
                }

                if(releases_from_source.length > 0)
                {
                    async.each(releases_from_source, function(r, eachCallback){

                        var release = digging.prepare_release($this, r);

                        if(release)
                        {
                            releases.push(release);
                        }
                        eachCallback(null);
                    }, function(err, res){
                        waterfall_callback(null);
                    });
                }
                else
                {
                    waterfall_callback(null);
                }
            }], function(err){
            search_artist_callback(releases);
        });
    },
    parse_list: function(html){

        var $this = this;

        var $ = cheerio.load(html);

        var str = $('div#wrapper div#wrapcontent.column div#content h1.inline').first().text().trim();

        if(str)
        {
            var split = str.split(' : ');
            if(split && split.length && split.length == 2)
            {
                var query = split[1];
            }
        }

        var releases = [];
        var releases_index = {};
        var all_tracks = [];

        if($('section.page-content-container div.tracks ul.bucket-items li.bucket-item').length > 0)
        {
            $('section.page-content-container div.tracks ul.bucket-items li.bucket-item').each(function(){
                var release_title = null;
                var track_title = null;
                var release_year = null;
                var release_date = null;
                var release_cover = null;
                var release_label = null;
                var release_url = null;
                var release_genre = null;
                var track_artist = null;
                var release_source_id = null;
                var track_source_id = $(this).attr('data-ec-id');
                var track_sample = 'https://geo-samples.beatport.com/lofi/'+track_source_id+'.LOFI.mp3';

                var artist_list = [];

                var a = $(this).find('p.buk-track-artists a').each(function(){
                    artist_list.push($(this).text().trim());
                });


                var l = $(this).find('p.buk-track-labels a').text().trim();
                if(l)
                {
                    release_label = l;
                }

                var t = $(this).find('div.buk-track-artwork-parent a').first();
                if(t)
                {
                    release_url = 'https://www.beatport.com'+t.attr('href');
                    var tab = t.attr('href').split('/');
                    var title = tab[tab.length-2];
                    title = S(title).replaceAll('-', ' ').s;
                    release_title = S(title).capitalize().s.trim();
                    release_cover = t.find('img').attr('data-src');
                    release_source_id = tab[tab.length-1];
                }

                var gs = [];
                $(this).find('p.buk-track-genre a').each(function(k, v){
                    var a = $(v);
                    gs.push(a.text());
                });

                if(gs.length > 0)
                {
                    release_genre = gs.join(', ');
                }

                var rd = $(this).find('p.buk-track-released').text().trim().split('-');

                if(rd && rd.length && rd.length == 3)
                {
                    release_year = rd[0];
                    var month = rd[1];
                    var day = rd[2];
                    release_date = release_year+'-'+month+'-'+day;
                }

                track_artist = artist_list;


                if($(this).find('p.buk-track-title a *'))
                {
                    var str = [];
                    $(this).find('p.buk-track-title a *').each(function(){
                        str.push($(this).text().trim());
                    });
                    track_title = str.join(' ');
                }

                if(track_source_id && release_source_id && track_title)
                {
                    all_tracks.push({
                        release_title : release_title,
                        track_title : track_title,
                        release_year : release_year,
                        release_date : release_date,
                        release_cover : release_cover,
                        release_label : release_label,
                        release_url : release_url,
                        release_genre : release_genre,
                        track_artist : track_artist,
                        release_source_id : release_source_id,
                        track_source_id : track_source_id,
                        track_sample : track_sample
                    });
                }
            });
        }

        all_tracks.forEach(function(t){
            var rid = t.release_source_id;

            if(!releases_index[rid])
            {
                releases_index[rid] = {
                    release_title : t.release_title,
                    release_year : t.release_year,
                    release_date : t.release_date,
                    release_cover : t.release_cover,
                    release_label : t.release_label,
                    release_url : t.release_url,
                    release_genre : t.release_genre,
                    release_source_id : t.release_source_id
                };
                releases_index[rid].tracks = [];
            }
            releases_index[rid].tracks.push({
                artist: t.track_artist,
                sample: t.track_sample,
                title: t.track_title
            });
        });

        for(var rid in releases_index) {
            var r = releases_index[rid];
            if(r.tracks)
            {
                var release_artist_list = [];

                r.tracks.forEach(function(t, kk){

                    var artist = t.artist;

                    releases_index[rid].tracks[kk].artist = artist.join(', ');

                    artist.forEach(function(the_artist, k){
                        if(!release_artist_list.indexOf(the_artist))
                        {
                            release_artist_list.push(the_artist);
                        }
                    });
                });

                if(release_artist_list.length > 1)
                {
                    releases_index[rid].release_artist = 'Various Artists';
                }
                else
                {
                    releases_index[rid].release_artist = release_artist_list[0];
                }
            }
        }

        for(var rid in releases_index) {
            var r = releases_index[rid];
            releases.push({kind: 'release', artist: r.release_artist, tracks: r.tracks, label: r.release_label, cover: r.release_cover, source: $this, url: r.release_url, title: r.release_title, played_by: null, year: r.release_year, release_date: r.release_date, genre: r.release_genre});
        }

        return releases;
    },
    get_news: function(query, get_news_callback){

        var $this = this;

        var d = new Date().toJSON().slice(0,10);
        var feeds = [
            "https://www.beatport.com/tracks/all?per-page=150&digging_date="+d,
            "https://www.beatport.com/tracks/all?per-page=150&genres=38&digging_date="+d,
            "https://www.beatport.com/tracks/all?per-page=150&genres=5&digging_date="+d,
        ];

        all_beatport = [];
        async.each(feeds, function(url, cb){
            digging_curl(url, false, false, function(error, result){
                if(result.html)
                {
                    var releases = $this.parse_list(result.html);

                    if(releases && releases.length && releases.length > 0)
                    {
                        releases.forEach(function(r){
                            var release = digging.prepare_release($this, r);
                            if(release)
                            {
                                all_beatport.push(release);
                            }
                        });
                    }
                    cb(null);
                }
                else
                {
                    cb(null)
                }
            });
        }, function(e, r){
            get_news_callback(all_beatport);
        });
    }
};
