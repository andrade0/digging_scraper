const cluster = require('../cluster.js');
const digging = require('../digging.js');
const _ = require('underscore');
const async = require( 'async' );
const S = require( 'string' );
const fs = require( 'fs' );
const cheerio = require( 'cheerio' );
const md5 = require( 'md5' );
const moment = require( 'moment' );
const Fuse = require( 'fuse.js' );

module.exports = {
    name: 'Resident advisor',
    value: 'ra',
    //Return sting image uri
    logo: 'public/img/sources/ra.png',
    //query: string
    //search_artist_1_callback: function, receiving array of object, ex: [{url: 'http://xxxx', x: y, ...}, {url: 'http://xxxx', x: y, ...}, ....]
    search_artist: function(query, href, search_artist_callback){

        var $this = this;
        var pages = [];

        var artist_url = query;
        artist_url = S(artist_url).replaceAll('(', '').s;
        artist_url = S(artist_url).replaceAll(')', '').s;
        artist_url = S(artist_url).humanize().s;
        artist_url = artist_url.replace(/[^\w\s]/gi, '');
        artist_url = S(artist_url).replaceAll(' ', '').s;
        artist_url = artist_url.toLowerCase();
        var url = "https://www.residentadvisor.net/dj/"+artist_url+"/top10";

        async.waterfall([
            function(waterfall_callback){
                digging_curl(url, false, false, waterfall_callback);
            },
            function(result, waterfall_callback) {
                if(result.html)
                {
                    var $ = cheerio.load(result.html);
                    $('li').each(function(){
                        if($(this) && $(this).attr('id') && $(this).attr('id').length > 0)
                        {
                            var expl = $(this).attr('id').split('-');
                            if(expl.length == 2)
                            {
                                if(expl[0]=='quantity')
                                {
                                    var playlistId = expl[1];
                                    pages.push({url: url+'?chart='+playlistId});
                                }
                            }
                        }
                    });
                    waterfall_callback(null);
                }
            }
        ], function(error, result){
            search_artist_callback(pages);
        });
    },
    //query: string
    //page: Object (ex: {url: 'http://xxxx', x: y, ....}
    //search_artist_1_callback: function, receiving release object or null
    search_artist_1: function(query, page, search_artist_1_callback){
        var $this = this;
        async.waterfall([
            function(waterfall_callback)
            {
                digging_curl(page.url, false, true, waterfall_callback);
            },
            function(result, waterfall_callback)
            {
                if(result.release)
                {
                    var release = digging.prepare_release($this, result.release);
                    waterfall_callback(release);
                }
                else if(result.html)
                {
                    var r = $this.parse_detail(result.html, page.url);

                    if(r)
                    {
                        var release = digging.prepare_release($this, r);
                        if(release)
                        {
                            waterfall_callback(release)
                        }
                        else
                        {
                            waterfall_callback(null);
                        }
                    }
                    else
                    {
                        waterfall_callback(null);
                    }
                }
                else
                {
                    waterfall_callback(null);
                }
            }
        ], function(err){
            search_artist_1_callback(err);
        });
    },
    //html : html source code
    //page_url : String (url of scraped page)
    parse_detail: function(html, url){
        var $ = cheerio.load(html);
        var sample = null;
        var ep = '';
        var cover = null;
        var label = null;
        var release_date = null;
        var source = "Resident advisor";
        var tracks = [];
        var year = null;
        var artist = null;

        artist = $('#sectionHead').find('h1').text().trim();

        if(!artist || artist && artist.length < 1)
        {
            artist = $('#featureHead').find('h1').text().trim();
        }

        $('ul#tracks li').each(function(k,v)
        {
            if(k == 1000)
            {

            }
            else
            {
                var track = {};
                $(this).find('div').each(function(k2, div)
                {
                    var classe = div.attribs.class;

                    if(classe == 'artist')
                    {
                        if (div.children && div.children.length > 0 && div.children[0] && div.children[0].name && div.children[0].name == 'a')
                        {
                            track.artist = div.children[0].children[0].data;
                        }
                        else if(div.children && div.children[0] && div.children[0].data && div.children[0].data.length > 0)
                        {
                            track.artist = div.children[0].data;
                        }
                        else
                        {
                            track.artist = '';
                        }
                    }
                    else if(classe == 'track')
                    {
                        if (div.children && div.children.length > 0 && div.children[0] && div.children[0].name && div.children[0].name == 'a')
                        {
                            track.title = div.children[0].children[0].data;
                        }
                        else if(div.children[0])
                        {
                            track.title = div.children[0].data;
                        }
                        else
                        {
                            track.title = null;
                        }
                    }
                    else if(classe == 'label')
                    {
                        if (div.children && div.children.length > 0 && div.children[0] && div.children[0].name && div.children[0].name == 'a')
                        {
                            track.label = div.children[0].children[0].data;
                        }
                        else
                        {
                            if(div.children && div.children.length > 0)
                            {
                                track.label = div.children[0].data;
                            }
                        }
                    }

                });
                tracks.push(track);
            }

        });

        var months = [];
        months.push('January');
        months.push('February');
        months.push('March');
        months.push('April');
        months.push('May');
        months.push('June');
        months.push('July');
        months.push('August');
        months.push('September');
        months.push('October');
        months.push('November');
        months.push('December');
        var release_date_str = $('section.content div.plus8 div.clearfix.pb16 div.dropdown.but.arrow-down').find('span').text();
        if(release_date_str)
        {
            var sp = release_date_str.split(' ');
            if (sp && sp.length && sp.length == 2) {
                var m = _.indexOf(months, sp[0]);
                if(m>0)
                {
                    if(m < 10)
                    {
                        m = '0'+m;
                    }
                    release_date = sp[1]+'-'+m+'-'+'01';
                }
            }
        }

        $('main ul.content-list li section.content.clearfix div.plus8 div.clearfix.pb16 div.dropdown.but.arrow-down span').each(function(k, v){
            if(v.children && v.children[0] && v.children[0].data)
            {
                var split = v.children[0].data.split(' ');
                if(split && split[1])
                {
                    if(artist)
                    {
                        ep += artist+' RA chart '+v.children[0].data;
                    }
                    else
                    {
                        ep += ' RA chart '+v.children[0].data;
                    }

                    year = split[1].trim();
                }
            }
        });

        if(tracks.length>0)
        {
            return {kind: 'playlist', artist: artist, tracks: tracks, label: label, cover: cover, source: source, url: url, title: ep, played_by: artist, year: year, genre: 'Electronic music', release_date: release_date};
        }
        else
        {
            return null;
        }
    }
};
