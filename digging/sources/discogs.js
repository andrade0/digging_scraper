const cluster = require('../cluster.js');
const digging = require('../digging.js');
const _ = require('underscore');
const async = require( 'async' );
const S = require( 'string' );
const fs = require( 'fs' );
const cheerio = require( 'cheerio' );
const md5 = require( 'md5' );
const moment = require( 'moment' );
const Fuse = require( 'fuse.js' );

module.exports = {
    name: 'Discogs',
    value: 'discogs',
    //Return sting image uri
    logo: 'public/img/sources/discogs.png',
    //query: string
    //search_artist_1_callback: function, receiving array of object, ex: [{url: 'http://xxxx', x: y, ...}, {url: 'http://xxxx', x: y, ...}, ....]
    search_artist: function(query, href, search_artist_callback){
        if(href)
        {
            href = href + '?limit=500';
        }
        var $this = this;
        var pages = [];
        async.waterfall([
            function(waterfall_callback)
            {
                if(href)
                {
                    waterfall_callback(null, null);
                }
                else
                {
                    var url = "http://www.discogs.com/search/?q="+encodeURI(query)+"&type=artist";
                    digging_curl(url, false, false, waterfall_callback);
                }
            },
            function(result, waterfall_callback)
            {
                if(href)
                {
                    waterfall_callback(null, href);
                }
                else
                {
                    var url = null;
                    if(result.html)
                    {
                        var discogs_results = [];
                        var $ = cheerio.load(result.html);
                        $('a.search_result_title').each(function(){
                            discogs_results.push({title: $(this).text().toLowerCase().trim(), href: 'https://www.discogs.com'+$(this).attr("href")});
                        });
                        var options = {
                            keys: ['title'],
                            id: 'href'
                        };

                        var fuse = new Fuse(discogs_results, options);
                        var fuse_result = fuse.search(query);

                        if(fuse_result.length > 0 && fuse_result[0])
                        {
                            url = fuse_result[0];
                        }

                        waterfall_callback(null, url);
                    }
                    else
                    {
                        waterfall_callback(true);
                    }
                }
            },
            function(url ,waterfall_callback)
            {
                if(url)
                {
                    digging_curl(url, false, false, waterfall_callback);
                }
                else
                {
                    waterfall_callback(true);
                }
            },
            function(result ,waterfall_callback)
            {
                if(result.html)
                {

                    var $ = cheerio.load(result.html);

                    $('#artist tr.card td.title > a').each(function(k, v){
                        var url = 'https://www.discogs.com'+$(v).attr('href');
                        pages.push({discogs_id: null, url: url});
                    });

                    $('#label tr.card td.title > a').each(function(k, v){
                        var url = 'https://www.discogs.com'+$(v).attr('href');
                        pages.push({discogs_id: null, url: url});
                    });

                    $('#search_results .card h4 .search_result_title').each(function(k, v){
                        var href = 'http://www.discogs.com'+$(v).attr('href');
                        pages.push({discogs_id: null, url: href});
                    });
                    waterfall_callback(null);
                }
                else
                {
                    waterfall_callback(true);
                }
            }
        ], function(err){
            search_artist_callback(pages);
        });
    },
    //query: string
    //page: Object (ex: {url: 'http://xxxx', x: y, ....}
    //search_artist_1_callback: function, receiving release object or null
    search_artist_1: function(query, page, search_artist_1_callback){
        var $this = this;
        async.waterfall([
            function(waterfall_callback)
            {
                digging_curl(page.url, true, true, waterfall_callback);
            },
            function(result, waterfall_callback)
            {
                if(result.release)
                {
                    var release = digging.prepare_release($this, result.release);
                    waterfall_callback(release);
                }
                else if(result.html)
                {
                    var r = $this.parse_detail(result.html, page.url);

                    if(r)
                    {
                        var release = digging.prepare_release($this, r);

                        if(release)
                        {
                            waterfall_callback(release)
                        }
                        else
                        {
                            waterfall_callback(null);
                        }
                    }
                    else
                    {
                        waterfall_callback(null);
                    }
                }
                else
                {
                    waterfall_callback(null);
                }
            }
        ], function(err){
            search_artist_1_callback(err);
        });
    },
    //html : html source code
    //page_url : String (url of scraped page)
    parse_detail: function(html, page_url){
        var $ = cheerio.load(html);
        var release = null;
        var artists_list = [];
        var release_cover = null;
        var release_title = null;
        var release_label = null;
        var release_year = null;
        var release_date = null;
        var release_genre = [];

        if($('.share_link').attr('data-url') && $('.share_link').attr('data-url').length)
        {
            //page_url = $('.share_link').attr('data-url');
        }

        $('#page_content').each(function(k, v){

            $(this).find('div.body').each(function(k,v){
                $(this).find('div.image_gallery.image_gallery_large a.thumbnail_link.thumbnail_size_large.thumbnail_orientation_nocrop span.thumbnail_center picture img').each(function(k,v){
                    if(v.attribs && v.attribs.src && v.attribs.src.trim().length > 0)
                    {
                        release_cover = S(v.attribs.src).trim().s;
                    }
                });

                $(this).find('div.profile').each(function(k,v){

                    $(this).find('#profile_title').find('span[itemprop=byArtist]').find('span').each(function(k,v){
                        if(v.attribs && v.attribs.title)
                        {
                            artists_list.push(v.attribs.title);
                        }
                    });

                    $(this).find('#profile_title').find('span[itemprop=name]').each(function(k,v){
                        if(v.children && v.children.length > 0 && v.children[0].data && v.children[0].data.trim().length > 0)
                        {
                            release_title = S(v.children[0].data).trim().s;
                        }
                    });

                    $(this).find('.content').find('a').each(function(k,v){
                        if(v.attribs && v.attribs.href)
                        {
                            var href = v.attribs.href;
                            if(href.toLowerCase().match(new RegExp('label',"i")))
                            {
                                href = href.replace('/label/','');
                                var ex = href.split('-');
                                if(ex[0] && ex[0].length && ex[1] && ex[1].length)
                                {
                                    ex[0]='';
                                    release_label = ex.join(' ').trim();
                                }
                            }

                            if(href.toLowerCase().match(new RegExp('year',"i")))
                            {
                                href = href.replace('/search/?','');
                                var ex = href.split('&');
                                if(ex[0] && ex[0].length && ex[1] && ex[1].length)
                                {
                                    release_year = ex[1].replace('year=','');
                                }
                            }

                            if(href.toLowerCase().match(new RegExp('genre',"i")))
                            {
                                release_genre.push($(v).text().trim());
                            }

                            if(href.toLowerCase().match(new RegExp('style',"i")))
                            {
                                release_genre.push($(v).text().trim());
                            }

                            if(href.toLowerCase().match(new RegExp('decade',"i")))
                            {
                                var months = [];
                                months.push('');
                                months.push('Jan');
                                months.push('Feb');
                                months.push('Mar');
                                months.push('Apr');
                                months.push('May');
                                months.push('Jun');
                                months.push('Jul');
                                months.push('Aug');
                                months.push('Sep');
                                months.push('Oct');
                                months.push('Nov');
                                months.push('Dec');
                                var release_date_str = $(v).text().trim();

                                if(release_date_str)
                                {
                                    var sp = release_date_str.split(' ');
                                    if(sp && sp.length && sp.length == 3)
                                    {
                                        var m =  _.indexOf(months, sp[1]);

                                        if(m>0)
                                        {
                                            if(m < 10)
                                            {
                                                m = '0'+m;
                                            }
                                            release_date = sp[2]+'-'+m+'-'+sp[0];
                                        }
                                    }
                                    else if(sp && sp.length && sp.length == 2)
                                    {
                                        var m =  _.indexOf(months, sp[0]);

                                        if(m>0)
                                        {
                                            if(m < 10)
                                            {
                                                m = '0'+m;
                                            }

                                            release_date = sp[1]+'-'+m+'-'+'01';
                                        }
                                    }

                                }
                            }
                        }
                    });

                });
            });

            var context = $(this);

            artists_list.forEach(function(release_artist){
                var tracks = [];
                if(release_artist && release_title)
                {
                    context.find('#tracklist').find('table.playlist').find('tr').each(function(k, v){

                        var title = null;

                        $(this).find('span.tracklist_track_title').each(function(k, v){
                            if(v.children && v.children[0] && v.children[0].data)
                            {
                                title = v.children[0].data.trim();
                            }
                        });

                        var artist = null;

                        $(this).find('td.tracklist_track_artists').find('a').each(function(k, v){
                            if(v.children && v.children.length > 0 && v.children[0].data && v.children[0].data.length > 0)
                            {
                                artist = S(v.children[0].data).trim().s;
                            }
                        });

                        var duration = null;
                        $(this).find('td.tracklist_track_duration').find('span').each(function(k, v){
                            if(v.children && v.children.length > 0 && v.children[0].data && v.children[0].data.length > 0)
                            {
                                duration = S($(v).text()).trim().s;
                            }
                        });

                        if(artist == null)
                        {
                            artist = release_artist;
                        }

                        if(artist && title)
                        {
                            tracks.push({  artist: artist, title: title, duration: duration });
                        }

                    });
                }

                if(tracks.length>0)
                {
                    if(release_artist == 'Various')
                    {
                        release_artist = null;
                    }

                    release = {kind: 'release',artist: release_artist, tracks: tracks, label: release_label, cover: release_cover, source: 'discogs', url: page_url, title: release_title, played_by: release_artist, year: release_year, genre: release_genre.join(', '), release_date: release_date};

                }
            });
        });
        return release;
    },
    //query: string
    //search_artist_1_callback: function, receiving array of object, ex: [{url: 'http://xxxx', x: y, ...}, {url: 'http://xxxx', x: y, ...}, ....]
    search_label: function(query, href, search_artist_callback){
        if(href)
        {
            href = href + '?limit=500';
        }
        var $this = this;
        var pages = [];
        async.waterfall([
            function(waterfall_callback)
            {
                if(href)
                {
                    waterfall_callback(null, null);
                }
                else
                {
                    var url = "http://www.discogs.com/search/?q="+encodeURI(query)+"&type=label";
                    digging_curl(url, false, false, waterfall_callback);
                }
            },
            function(result, waterfall_callback)
            {
                if(href)
                {
                    waterfall_callback(null, href);
                }
                else
                {
                    var url = null;
                    if(result.html)
                    {
                        var discogs_results = [];
                        var $ = cheerio.load(result.html);
                        $('a.search_result_title').each(function(){
                            discogs_results.push({title: $(this).text().toLowerCase().trim(), href: 'https://www.discogs.com'+$(this).attr("href")});
                        });
                        var options = {
                            keys: ['title'],
                            id: 'href'
                        };
                        var fuse = new Fuse(discogs_results, options);
                        var fuse_result = fuse.search(query);

                        if(fuse_result.length > 0 && fuse_result[0])
                        {
                            url = fuse_result[0];
                        }

                        waterfall_callback(null, url);
                    }
                    else
                    {
                        waterfall_callback(true);
                    }
                }
            },
            function(url ,waterfall_callback)
            {
                if(url)
                {
                    digging_curl(url, false, false, waterfall_callback);
                }
                else
                {
                    waterfall_callback(true);
                }
            },
            function(result ,waterfall_callback)
            {
                if(result.html)
                {

                    var $ = cheerio.load(result.html);

                    $('#artist tr.card td.title > a').each(function(k, v){
                        var url = 'https://www.discogs.com'+$(v).attr('href');
                        pages.push({discogs_id: null, url: url});
                    });

                    $('#label tr.card td.title > a').each(function(k, v){
                        var url = 'https://www.discogs.com'+$(v).attr('href');
                        pages.push({discogs_id: null, url: url});
                    });

                    $('#search_results .card h4 .search_result_title').each(function(k, v){
                        var href = 'http://www.discogs.com'+$(v).attr('href');
                        pages.push({discogs_id: null, url: href});
                    });
                    waterfall_callback(null);
                }
                else
                {
                    waterfall_callback(true);
                }
            }
        ], function(err){
            search_artist_callback(pages);
        });
    },
    //query: string
    //page: Object (ex: {url: 'http://xxxx', x: y, ....}
    //search_artist_1_callback: function, receiving release object or null
    search_label_1: function(query, page, search_artist_1_callback){
        var $this = this;
        async.waterfall([
            function(waterfall_callback)
            {
                digging_curl(page.url, true, true, waterfall_callback);
            },
            function(result, waterfall_callback)
            {
                if(result.release)
                {
                    var release = digging.prepare_release($this, result.release);
                    waterfall_callback(release);
                }
                else if(result.html)
                {
                    var r = $this.parse_detail(result.html, page.url);
                    if(r)
                    {
                        var release = digging.prepare_release($this, r);

                        if(release)
                        {
                            waterfall_callback(release)
                        }
                        else
                        {
                            waterfall_callback(null);
                        }
                    }
                    else
                    {
                        waterfall_callback(null);
                    }
                }
                else
                {
                    waterfall_callback(null);
                }
            }
        ], function(err){
            search_artist_1_callback(err);
        });
    },
    get_release: function(url, callback){
        var $this = this;
        digging_curl(url, false, true, function(error, result){
            if(result.html) {
                var r = $this.parse_detail(result.html, url);
                if (r) {
                    var release = digging.prepare_release($this, r);
                    callback(release);
                }
                else
                {
                    callback(null);
                }
            }
            else
            {
                callback(null);
            }
        });
    },
    get_suggestion: function(track, callback)
    {
        var $this = this;
        digging_curl('https://www.discogs.com/fr/search/?q='+encodeURI(track.artist+' '+track.title)+'&type=release', false, false, function(error, result){
            if(result.html) {

                var $ = cheerio.load(result.html);

                var href = $('a.search_result_title').first().attr('href');

                if(href)
                {
                    var expl = $('a.search_result_title').first().attr('href').split('/');

                    var id = expl[expl.length - 1];

                    digging_curl('https://www.discogs.com/release/recs/'+id+'?type=release&page=1', false, false, function(error, result){
                        var r = $this.parse_suggestions(result.html, function(releases){
                            if (releases) {
                                callback(releases);
                            }
                            else
                            {
                                callback(null);
                            }
                        });
                    });
                }
                else
                {
                    callback(null);
                }
            }
            else
            {
                callback(null);
            }
        });
    },
    parse_suggestions: function(html, callback){
        var $this = this;
        var $ = cheerio.load(html);
        var release_url = $('div.card').first().find('a.thumbnail_link').first().attr('href');

        var releases = [];
        async.each($('div.card'), function(element, cb){
            var release_url = $(element).find('a.thumbnail_link').first().attr('href');
            var suggestion_url = 'https://www.discogs.com'+release_url;
            digging_curl(suggestion_url, true, true, function(error, result){
                if(result.release)
                {
                    releases.push(release);
                    cb(null);
                }
                else if(result.html)
                {
                    var r = $this.parse_detail(result.html, suggestion_url);
                    if (r) {
                        var release = digging.prepare_release($this, r);
                        releases.push(release);
                        cb(null);
                    }
                    else
                    {
                        cb(null);
                    }
                }
                else
                {
                    cb(null);
                }
            });
        }, function(e,r){
            callback(releases);
        });
    }/*,
    get_news: function(get_news_callback){
        var $this = this;
        var pages = [];
        async.waterfall([
            function(waterfall_callback)
            {
                var url = "https://www.discogs.com/?limit=50&page=1";
            digging_curl(url, false, waterfall_callback);
            },
            function(result ,waterfall_callback)
            {
                if(result.html)
                {
                    var $ = cheerio.load(result.html);

                    $('#search_results div.card > a').each(function(k, v){
                        var url = 'https://www.discogs.com'+$(v).attr('href');
                        pages.push({discogs_id: null, url: url});
                    });

                    waterfall_callback(null);
                }
                else
                {
                    waterfall_callback(true);
                }
            },
            function(waterfall_callback)
             {
            digging_curl(page.url, false, waterfall_callback);
             },
             function(result, waterfall_callback)
             {
             if(result.html)
             {
             var r = $this.parse_detail(result.html, page.url);
             if(r)
             {
             var release = digging.prepare_release($this, r);

             if(release)
             {
             waterfall_callback(release)
             }
             else
             {
             waterfall_callback(null);
             }
             }
             else
             {
             waterfall_callback(null);
             }
             }
             else
             {
             waterfall_callback(null);
             }
             }
        ], function(err){
            console.log(pages);
            //get_news_callback(pages);
        });
    }*/
};
