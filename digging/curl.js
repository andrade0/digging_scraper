var request = require('request');
var async = require('async');
const md5 = require( 'md5' );


digging_process_curl = function(url, wait, callback){

  wait = true;

    var start = Math.floor(Date.now() / 1000);

    try {

        var callback_sent = false;

        setTimeout(function(){

            if (callback_sent == false) {
                callback_sent = true;
                var end = Math.floor(Date.now() / 1000);
                var elased_time = end-start;
                console.log(elased_time+' - TIMEOUT : '+url);
                callback(null, {html: false, url: url});
            }

        }, 20000);

        url = url.replace('é','e');
        url = url.replace('è','e');
        url = url.replace('ê','e');

      var sleeps = [];
      sleeps.push(0.9);
      sleeps.push(1.8);
      sleeps.push(1.8);
      sleeps.push(1.2);
      sleeps.push(1.6);
      sleeps.push(3.6);
      sleeps.push(2.9);
      sleeps.push(0.95);
      sleeps.push(1.55);
      sleeps.push(2.1);
      sleeps.push(1.7);
      sleeps.push(1.5);
      sleeps.push(1.7);

      var user_agents = [];
      user_agents.push("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:36.0) Gecko/20100101 Firefox/36.0");
      user_agents.push("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0");
      user_agents.push("Mozilla/5.0 (Windows NT 6.1; rv:15.0) Gecko/20120716 Firefox/15.0a2");
      user_agents.push("Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; ko; rv:1.9.1b2) Gecko/20081201 Firefox/3.1b2");
      user_agents.push("Mozilla/5.0 (Windows; U; Windows NT 5.0; it-IT; rv:1.7.12) Gecko/20050915");
      user_agents.push("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.12 Safari/537.36 OPR/14.0.1116.4");
      user_agents.push("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.29 Safari/537.36 OPR/15.0.1147.24 (Edition Next)");
      user_agents.push("Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.0 (KHTML, like Gecko) Chrome/3.0.195.27 Safari/532.0");
      user_agents.push("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36");
      user_agents.push("Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; ko; rv:1.9.1b2) Gecko/20081201 Firefox/3.1b2");
      user_agents.push("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0");
      user_agents.push("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36");
      user_agents.push("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:36.0) Gecko/20100101 Firefox/36.0");
      user_agents.push("Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; ko; rv:1.9.1b2) Gecko/20081201 Firefox/3.1b2");

      var random_index = Math.floor((Math.random() * 10));
      var user_agent = user_agents[random_index];

      var curl_process = function(){
        console.log('Start curl : '+url);
        request(url, {headers: {
          "User-Agent": user_agent,
          "timeout": 2000
        }}, function (error, response, html)
        {
          var end = Math.floor(Date.now() / 1000);
          var elased_time = end-start;
          console.log(elased_time+' - finish curl : '+url);
          if(error)
          {
            var curl_result = {html: false, url: url};
          }
          else
          {
            var curl_result = {html: html, url: url};
          }

          if(callback_sent == false)
          {
            callback_sent = true;
            callback(null, curl_result);
          }
        });
      };

      if(wait)
      {
        var selected_time = sleeps[random_index];
        setTimeout(function(){
          curl_process();
        },1000*selected_time);
      }
      else
      {
        curl_process();
      }
    }
    catch (e) {
        callback(null, {html: false, url: url});
    }

};


digging_curl = function(url, wait, check_release, callback){

    //check_release = false; //delete on prod

    if(url)
    {
      digging_process_curl(url, wait, callback);
    }
    else
    {
        console.log("failed curl : "+url);
        callback(null, {html: false, url: url})
    }
};